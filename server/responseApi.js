/**
 * Creates a response object at success.
 */
function success(code, results, message) {
    return {
        message,
        code,
        error: false,
        ...results,
    };
}

/**
 * Creates a response object at error.
 */
function error(code, message) {
    return {
        message,
        code,
        error: true,
    };
}

module.exports = { success, error };
