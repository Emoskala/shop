const express = require("express");
const Router = express.Router();
const shopService = require("./shop.service");
const { success } = require("../../responseApi");

Router.get("/get-shop-items", getShopItems);

/**
 * Function for /get-shop-items route.
 */
function getShopItems(req, res, next) {
    shopService
        .getItems()
        .then((data) => {
            res.send(success(200, { products: data }, "Sent a shopping items"));
        })
        .catch((err) => next(err));
}

module.exports = Router;
