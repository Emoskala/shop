const mongoose = require("mongoose");
const { Schema } = mongoose;

const shopItemSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required: true,
    },
    unitPrice: {
        type: Number,
        required: true,
    },
});

module.exports = mongoose.model("ShopItems", shopItemSchema);
