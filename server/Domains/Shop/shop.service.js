const shopItemModel = require("./shop.item.model");

/**
 * Returns all items from shop.
 */
function getItems() {
    return shopItemModel.find();
}

module.exports = { getItems };
