const express = require("express");
const Router = express.Router();
const userService = require("./user.service");
const { success } = require("../../responseApi");

Router.post("/sign-in", signIn);
Router.post("/refresh-token", refreshToken);
Router.post("/create-user", createUser);

/**
 * Function for /sign-in route.
 */
function signIn(req, res, next) {
    const { username, password } = req.body;

    userService
        .authenticate(username, password)
        .then((data) => {
            res.send(success(200, data, "Signed in"));
        })
        .catch((err) => next(err));
}

/**
 * Function for /create-user route.
 */
function createUser(req, res, next) {
    const { username, password } = req.body;

    userService
        .createUser(username, password)
        .then((data) => {
            res.send(success(200, data, "Created account succesfully"));
        })
        .catch((err) => next(err));
}

/**
 * Function for /refresh-token route.
 */
function refreshToken(req, res, next) {
    const { refreshToken } = req.body;

    userService
        .refreshToken(refreshToken)
        .then((data) => {
            res.send(success(200, data, "Refreshed Token"));
        })
        .catch((err) => next(err));
}

module.exports = Router;
