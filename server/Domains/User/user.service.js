const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const UserModel = require("./user.model");

require("dotenv").config({ path: "../.env" });
const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;
const JWT_REFRESH_TOKEN_SECRET_KEY = process.env.JWT_REFRESH_TOKEN_SECRET_KEY;

/**
 * Authenticate a user.
 */
async function authenticate(username, password) {
    const user = await getUser(username);
    const res = user && bcrypt.compareSync(password, user.password);
    if (res) {
        return { accessToken: generateAccessToken(username), refreshToken: generateRefreshToken(username), username: username };
    } else {
        throw new Error("Invalid data!");
    }
}

/**
 * Create a new user.
 */
async function createUser(username, password) {
    const validationResult = await validateUsername(username);
    if (!validationResult) {
        await new UserModel({ username, password }).save();
        return { accessToken: generateAccessToken(username), refreshToken: generateRefreshToken(username), username: username };
    } else {
        throw new Error("Username already exists!");
    }
}

/**
 * Verifies delivered refresh token and refreshes access token.
 */
async function refreshToken(token) {
    return jwt.verify(token, JWT_REFRESH_TOKEN_SECRET_KEY, function (err, res) {
        if (err) {
            throw new Error(err);
        } else {
            return { accessToken: generateAccessToken(res.username), username: res.username };
        }
    });
}

/**
 * Returns a user with given username.
 */
async function getUser(username) {
    return await UserModel.findOne({ username: username });
}

/**
 * validates username.
 */
async function validateUsername(username) {
    return await UserModel.findOne({ username: username }).then((res) => Boolean(res));
}

/**
 * generates a new access token.
 */
function generateAccessToken(username) {
    return jwt.sign({ username }, JWT_SECRET_KEY, { expiresIn: "15m" });
}

/**
 * generates a new refresh token.
 */
function generateRefreshToken(username) {
    return jwt.sign({ username }, JWT_REFRESH_TOKEN_SECRET_KEY, { expiresIn: "30d" });
}

module.exports = {
    authenticate,
    createUser,
    refreshToken,
};
