const express = require("express");
const morgan = require("morgan");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const { jwt } = require("./middlewares/authorization");

//ENV
require("dotenv").config({ path: "./.env" });
const PORT = process.env.PORT || 6000;

//APP
const app = express();

//DB CONNECTION
mongoose.connect("mongodb://localhost/shop", { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
    console.log("connected to MongoDB");
});

//MIDDLEWARES
app.use(cors());
app.use(morgan("common"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use("/shop", jwt());

//ROUTES
const login = require("./Domains/User/user.controller");
const shop = require("./Domains/Shop/shop.controller");

app.use("/login", login);
app.use("/shop", shop);

//ERROR HANDLER
const { error } = require("./responseApi");

app.use((err, req, res, next) => {
    if (err.name === "UnauthorizedError") {
        res.status(401).send(json(401, "Invalid Token"));
    }
    res.status(400).json(error(400, err.message));
});

app.listen(PORT, function () {
    console.log("Server is listening at port " + PORT);
});
