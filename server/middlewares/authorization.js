const expressJwt = require("express-jwt");
require("dotenv").config({ path: "./.env" });
const JWT_SECRET_KEY = process.env.JWT_SECRET_KEY;

/**
 * Verifies delivered access token.
 */
const jwt = function () {
    return expressJwt({ secret: JWT_SECRET_KEY, algorithms: ["HS256"], isRevoked, getToken });
};

/**
 * Returns a token from request.
 */
const getToken = (req) => {
    return req.query.accessToken || req.body.accessToken;
};

const isRevoked = (req, payload, done) => {
    done();
};

module.exports = { jwt };
