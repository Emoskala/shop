/**
 * Selector which returns a shop products from store.
 */
export const getShopItems = (state) => {
    return state.shop.products;
};

/**
 * Selector which returns a item from store from delivered _id.
 */
export const getShopItem = (state, _id) => {
    const { products } = state.shop;

    return products.reduce((acc, item) => {
        if (item._id === _id) {
            return item;
        }
        return acc;
    }, {});
};

/**
 * Selector which returns an index of basket item from delivered _id.
 */
export const getBasketItemId = (state, _id) => {
    return state.findIndex((item) => {
        return item._id === _id;
    });
};

/**
 * Selector which returns all basket items.
 */
export const getItemsFromBasket = (state) => {
    const { basket } = state;
    const { products } = state.shop;

    return products.reduce((acc, item) => {
        const itemFound = basket.find((basketItem) => {
            return basketItem._id === item._id;
        });
        if (itemFound) {
            return [...acc, { ...item, quantity: itemFound.quantity }];
        } else {
            return acc;
        }
    }, []);
};
