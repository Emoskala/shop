import jwt_decode from "jwt-decode";
import { refreshAccessToken } from "./login";

/**
 * Checks if access token and refresh token are still valid.
 */
const handleTokenExpirationTime = async function (req) {
    const refreshToken = localStorage.getItem("refreshToken");
    const accessToken = localStorage.getItem("accessToken");

    if (!(jwt_decode(accessToken).exp * 1000 >= Date.now()) && !(jwt_decode(refreshToken).exp * 1000 >= Date.now())) {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");
        return window.location.reload();
    } else if (!(jwt_decode(accessToken).exp * 1000 >= Date.now()) && jwt_decode(refreshToken).exp * 1000 >= Date.now()) {
        await refreshAccessToken()
            .then((res) => {
                const { error } = res.data;
                if (error) {
                    window.location.reload();
                }
            })
            .catch((err) => console.log(err));
    }
    return req;
};

const handleFail = function (error) {
    throw new Error(error);
};

/**
 * Adds a token to request.
 */
const handleAddToken = function (req) {
    req.params.accessToken = localStorage.getItem("accessToken");
    return req;
};

/**
 * converts object to JSON
 */
const handleStringifyData = function (req) {
    JSON.stringify(req.data);
    return req;
};

export { handleStringifyData, handleTokenExpirationTime, handleAddToken, handleFail };
