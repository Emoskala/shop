import axios from "axios";
import { handleLoginApi, handleFail } from "./rootInterceptor";

const instance = axios.create({
    baseURL: "http://localhost:5000/login",
    headers: {
        "Content-Type": "application/json",
    },
});

instance.interceptors.request.use(handleLoginApi, handleFail);

function signIn(username, password) {
    return instance({
        method: "POST",
        url: "/sign-in",
        data: { username, password },
    })
        .then((res) => {
            return handleResponse(res);
        })
        .catch((err) => err.response);
}

function createUser(username, password) {
    return instance({
        method: "POST",
        url: "/create-user",
        data: { username, password },
    })
        .then((res) => {
            return handleResponse(res);
        })
        .catch((err) => err.response);
}

function refreshAccessToken() {
    return instance({
        method: "POST",
        url: "/refresh-token",
        data: { refreshToken: localStorage.getItem("refreshToken") },
    })
        .then((res) => {
            return handleResponse(res);
        })
        .catch((err) => err.response);
}

export { createUser, signIn, refreshAccessToken };

function handleResponse(res) {
    const { accessToken, refreshToken } = res.data;

    accessToken && localStorage.setItem("accessToken", accessToken);
    refreshToken && localStorage.setItem("refreshToken", refreshToken);
    return res;
}
