import axios from "axios";
import { handleShopApi, handleFail } from "./rootInterceptor";

const instance = axios.create({
    baseURL: "http://localhost:5000/shop",
    headers: {
        "Content-Type": "application/json",
    },
});

instance.interceptors.request.use(handleShopApi, handleFail);

function getShopItems() {
    return instance({
        method: "GET",
        url: "/get-shop-items",
        params: {},
    }).catch((err) => err.response);
}

export { getShopItems };
