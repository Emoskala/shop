import { handleTokenExpirationTime, handleAddToken, handleStringifyData, handleFail as Fail } from "./interceptors";

export const handleShopApi = async function (req) {
    req = await handleTokenExpirationTime(req);
    req = handleAddToken(req);
    req = handleStringifyData(req);
    return req;
};

export const handleLoginApi = function (req) {
    req = handleStringifyData(req);
    return req;
};

export const handleFail = Fail;
