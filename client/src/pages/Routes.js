import React from "react";
import { Route, Redirect } from "react-router-dom";
import MenuSwitch from "../components/Header/MenuSwitch";
import Home from "./Home/Home";
import Basket from "./Basket/Basket";
import Shop from "./Shop/Shop";

function Routes() {
    return (
        <section className="content">
            <MenuSwitch>
                <Route exact path="/home">
                    <Home />
                </Route>
                <Route exact path="/basket">
                    <Basket />
                </Route>
                <Route exact path="/shop">
                    <Shop />
                </Route>
                <Route path="/">
                    <Redirect to="/home" />
                </Route>
            </MenuSwitch>
        </section>
    );
}

export default Routes;
