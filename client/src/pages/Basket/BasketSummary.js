import React, { useState, useEffect } from "react";
import Price from "../../components/Price/Price";

function BasketSummary({ items }) {
    const [totalPrice, setTotalPrice] = useState(0);

    /**
     * If items have changed, it's calculating new total price.
     */
    useEffect(() => {
        const totalPrice = items.reduce((acc, { quantity, unitPrice }) => {
            return acc + quantity * unitPrice;
        }, 0);
        setTotalPrice(Math.round(totalPrice * 100) / 100);
    }, [items]);

    return (
        <div className="basket__summary-table">
            <div className="basket__price">
                <Price>total price: {totalPrice}</Price>
            </div>
        </div>
    );
}

export default BasketSummary;
