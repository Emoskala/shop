import React, { useState } from "react";
import Text from "../../components/Text/Text";
import Price from "../../components/Price/Price";
import TrashButton from "../../components/Buttons/TrashButton";
import EditButton from "../../components/Buttons/EditButton";
import ProductPanel from "../../components/ProductPanel/ProductPanel";
import ProductItem from "../../components/ProductItem/ProductItem";
import { useDispatch } from "react-redux";
import { basketSlice } from "../../reducers/basketSlice";

function BasketItem({ title, image, quantity, description, unitPrice, _id }) {
    const [isItemPanelActive, toggleIsItemPanelActive] = useState(false);
    const dispatch = useDispatch();

    /**
     * Removes item from basket.
     */
    const handleRemoveItemFromBasket = () => {
        dispatch(basketSlice.actions.removeItemFromBasket(_id));
    };

    /**
     * Handles toggle ProductPanel
     */
    const handleTogglePanel = () => {
        toggleIsItemPanelActive(!isItemPanelActive);
    };
    return (
        <>
            <ProductItem title={title} description={description} image={image} className="basket">
                <Text>Quantity: {quantity} </Text>
                <Price>Price: {Math.round(unitPrice * quantity * 100) / 100}</Price>
                <EditButton className="basket__edit-button" onClick={handleTogglePanel} />
                <TrashButton className="basket__trash-button" onClick={handleRemoveItemFromBasket} />
            </ProductItem>
            {isItemPanelActive && <ProductPanel handleTogglePanel={handleTogglePanel} _id={_id} image={image} quantityToEdit={quantity} unitPrice={unitPrice} />}
        </>
    );
}

export default BasketItem;
