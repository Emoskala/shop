import React from "react";
import { useSelector } from "react-redux";
import { getItemsFromBasket } from "../../selectors/shopSelector";
import BasketItem from "./BasketItem";
import BasketSummary from "./BasketSummary";
import Text from "../../components/Text/Text";

function Basket() {
    const itemsFromBasket = useSelector(getItemsFromBasket);

    /**
     * Creates all basketItem components.
     */
    const createBasketItems = () => {
        return itemsFromBasket.map((item) => {
            const { image, quantity, title, description, unitPrice, _id } = item;
            return <BasketItem title={title} description={description} quantity={quantity} unitPrice={unitPrice} image={image} key={_id} _id={_id} />;
        });
    };

    return (
        <div className="basket-block">
            <div className="basket">
                {itemsFromBasket.length ? (
                    <>
                        <ul className="basket__list">{createBasketItems()}</ul>
                        <BasketSummary items={itemsFromBasket} />
                    </>
                ) : (
                    <Text>Your basket is empty</Text>
                )}
            </div>
        </div>
    );
}

export default Basket;
