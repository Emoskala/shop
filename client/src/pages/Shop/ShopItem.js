import React, { useState } from "react";

import BasketButton from "../../components/Buttons/BasketButton";
import Price from "../../components/Price/Price";
import ProductPanel from "../../components/ProductPanel/ProductPanel";
import ProductItem from "../../components/ProductItem/ProductItem";

function ShopItem({ title, description, image, _id, unitPrice }) {
    const [isItemPanelActive, toggleIsItemPanelActive] = useState(false);

    /**
     * Handles toggle ProductPanel
     */
    const handleToggleItemPanelActive = () => {
        toggleIsItemPanelActive(!isItemPanelActive);
    };

    return (
        <>
            <ProductItem title={title} image={image} description={description} className="shop">
                <Price>{unitPrice}</Price>
                <BasketButton onClick={handleToggleItemPanelActive} className="shop__basket-button" />
            </ProductItem>
            {isItemPanelActive && <ProductPanel handleTogglePanel={handleToggleItemPanelActive} _id={_id} image={image} unitPrice={unitPrice} />}
        </>
    );
}

export default ShopItem;
