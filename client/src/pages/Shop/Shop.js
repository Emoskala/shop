import React from "react";

import ShoppingList from "./ShoppingList";

function Shop() {
    return (
        <div className="shop">
            <ShoppingList />
        </div>
    );
}

export default Shop;
