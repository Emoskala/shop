import React from "react";
import { useSelector } from "react-redux";
import { getShopItems } from "../../selectors/shopSelector";
import ShopItem from "./ShopItem";

function ShoppingList() {
    const shoppingListItems = useSelector(getShopItems);

    /**
     * Creates all ShopItem components.
     */
    const createShoppingListItems = () => {
        return shoppingListItems.map(({ title, description, image, unitPrice, _id }) => {
            return <ShopItem title={title} description={description} image={image} key={_id} _id={_id} unitPrice={unitPrice} />;
        });
    };

    return (
        <div className="shop__shopping-list">
            <ul className="shop__list">{createShoppingListItems()}</ul>
        </div>
    );
}

export default ShoppingList;
