import React from "react";

import Text from "../../components/Text/Text";

function Home() {
    return (
        <div className="home">
            <Text>Home!</Text>
        </div>
    );
}

export default Home;
