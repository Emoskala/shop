import React, { useEffect } from "react";
import HeaderContainer from "../components/Header/Header";
import MenuItem from "../components/Header/MenuItem";
import Menu from "../components/Header/Menu";
import LogoutButton from "../components/Buttons/LogoutButton";
import { addShopItems } from "../middlewares/shop";
import { useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";
import { userSlice } from "../reducers/userSlice";

function Header() {
    const location = useLocation();
    const dispatch = useDispatch();

    /**
     * Logout from ShopApp and removes tokens.
     */
    const handleLogout = () => {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");
        dispatch(userSlice.actions.logout());
    };

    useEffect(() => {
        dispatch(addShopItems());
    }, []);

    return (
        <HeaderContainer>
            <Menu>
                <MenuItem LinkTo={"/home"} active={location.pathname === "/home"}>
                    Home
                </MenuItem>
                <MenuItem LinkTo={"/shop"} active={location.pathname === "/shop"}>
                    Shop
                </MenuItem>
                <MenuItem LinkTo={"/basket"} active={location.pathname === "/basket"}>
                    Basket
                </MenuItem>
                <LogoutButton className="header__logout-button" onClick={handleLogout} />
            </Menu>
        </HeaderContainer>
    );
}

export default Header;
