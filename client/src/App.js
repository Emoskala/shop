import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import Login from "./components/Login/Login";
import Header from "./pages/Header";
import Routes from "./pages/Routes";
import { handleUserAtRender } from "./middlewares/login";
import Loader from "./components/Loader/Loader";
import Error from "./components/Error/Error";

/**
 * Creates a component which contains the Shop app.
 */
function App() {
    const loggedIn = useSelector((state) => state.user.loggedIn);
    const dispatch = useDispatch();

    /**
     * Checks if the user has still valid refreshToken in localStorage.
     */
    useEffect(() => {
        const token = localStorage.getItem("refreshToken");
        if (token && token !== "undefined") {
            dispatch(handleUserAtRender());
        }
    }, []);

    return (
        <div className="app">
            {loggedIn && (
                <Router>
                    <Header />
                    <Routes />
                </Router>
            )}
            {!loggedIn && <Login />}
            <Loader />
            <Error />
        </div>
    );
}

export default App;
