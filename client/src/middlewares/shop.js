import { getShopItems } from "../axios/api";
import { shopSlice } from "../reducers/shopSlice";

/**
 * Handles a request for shopping list.
 */
export const addShopItems = () => {
    return (dispatch) => {
        dispatch(shopSlice.actions.addShopItemsPending());
        getShopItems().then((res) => {
            const { error, message } = res.data;
            if (error) {
                dispatch(shopSlice.actions.addShopItemsError(message));
            } else {
                const { products } = res.data;
                dispatch(shopSlice.actions.addShopItems(products));
            }
        });
    };
};
