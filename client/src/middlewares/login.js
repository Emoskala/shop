import { signIn, createUser, refreshAccessToken } from "../axios/login";
import { userSlice } from "../reducers/userSlice";

export const signInUser = (username, password) => {
    return (dispatch) => {
        handleRequest(signIn, dispatch, username, password);
    };
};

export const registerUser = (username, password) => {
    return (dispatch) => {
        handleRequest(createUser, dispatch, username, password);
    };
};

export const handleUserAtRender = () => {
    return (dispatch) => {
        handleRequest(refreshAccessToken, dispatch);
    };
};

/**
 * Common function for middlewares which handles a login.
 */
function handleRequest(req, dispatch, username, password) {
    dispatch(userSlice.actions.loginPending());
    req(username, password).then((res) => {
        const { error, message, username } = res.data;
        if (error) {
            dispatch(userSlice.actions.loginError(message));
        } else {
            dispatch(userSlice.actions.login(username));
        }
    });
}
