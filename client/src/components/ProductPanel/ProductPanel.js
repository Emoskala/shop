import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useEventListener } from "../../hooks/useEventListener";
import { basketSlice } from "../../reducers/basketSlice";
import PrimaryButton from "../Buttons/PrimaryButton";
import CloseButton from "../Buttons/CloseButton";
import Price from "../Price/Price";
import Text from "../Text/Text";
import Image from "../Image/Image";

function ProductPanel({ handleTogglePanel, image, unitPrice, quantityToEdit, _id }) {
    const [quantity, setQuantity] = useState(1);
    const dispatch = useDispatch();

    /**
     * Checks if user edit or create new basket item.
     */
    useEffect(() => {
        quantityToEdit && setQuantity(quantityToEdit);
    }, []);

    /**
     * changes actual value of state.
     */
    const handleInputValue = (event) => {
        if (event.target.value === "") {
            setQuantity("");
        } else {
            event.target.value >= 1 && event.target.value <= 100 && setQuantity(Number(event.target.value));
        }
    };

    /**
     * Event listener method.
     */
    const handleKeyDown = (event) => {
        if (event.code === "Escape") {
            handleTogglePanel();
        }
    };

    /**
     * Adds item to basket.
     */
    const proceedAddToBasket = () => {
        dispatch(basketSlice.actions.addItemToBasket({ _id, quantity }));
        handleTogglePanel();
    };

    useEventListener("keydown", handleKeyDown);

    return (
        <div className="panel-block">
            <div className="panel-block__panel">
                <div className="panel-block__image-block">
                    <Image image={image} />
                </div>
                <div className="panel-block__description-block">
                    <label>
                        <Text>Quantity</Text>
                    </label>
                    <input type="number" value={quantity} onChange={handleInputValue} className="panel-block__input" />
                    <Text>Price to pay:</Text>
                    <Price>{Math.round(unitPrice * quantity * 100) / 100}</Price>
                    <PrimaryButton className="panel-block__primary-button" onClick={proceedAddToBasket}>
                        Add to basket
                    </PrimaryButton>
                </div>
                <CloseButton className="panel-block__close-button" onClick={handleTogglePanel} />
            </div>
        </div>
    );
}

export default ProductPanel;
