import React from "react";
import ErrorPage from "./ErrorPage";
import { useSelector } from "react-redux";

/**
 * Creates Error component which handles errors.
 */
function Error() {
    const state = useSelector((state) => state);

    return (
        <>
            {state.user.error && <ErrorPage errorMessage={state.user.error} errorSource={"user"} />}
            {state.shop.error && <ErrorPage errorMessage={state.shop.error} errorSource={"shop"} />}
        </>
    );
}

export default Error;
