import React from "react";
import Title from "../../components/Title/Title";
import Text from "../../components/Text/Text";
import CloseButton from "../Buttons/CloseButton";
import { FaExclamationTriangle } from "react-icons/fa";
import { useDispatch } from "react-redux";
import { userSlice } from "../../reducers/userSlice";
import { shopSlice } from "../../reducers/shopSlice";

function ErrorPage({ errorMessage, errorSource }) {
    const dispatch = useDispatch();

    /**
     * Disables error message.
     */
    const handleDisableError = () => {
        errorSource === "shop" && dispatch(shopSlice.actions.addShopItemsDisableError());
        errorSource === "user" && dispatch(userSlice.actions.loginDisableError());
    };

    return (
        <div className="error-block">
            <div className="error">
                <div className="error__icon">
                    <FaExclamationTriangle />
                </div>
                <Title>Something went wrong!</Title>
                <div className="error__message">
                    <Text>{errorMessage}</Text>
                </div>
                <CloseButton onClick={handleDisableError} className="error__button" />
            </div>
        </div>
    );
}

export default ErrorPage;
