import React from "react";

function Input({ name, type, className, placeholder, value, onChange }) {
    return <input onChange={onChange} type={type} name={name} className={`input ${className}`} value={value} placeholder={placeholder} />;
}

export default Input;
