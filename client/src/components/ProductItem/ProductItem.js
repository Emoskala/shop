import React from "react";
import Text from "../Text/Text";
import Title from "../Title/Title";
import Thumbnail from "../Thumbnail/Thumbnail";

function ProductItem({ title, description, image, className, children }) {
    return (
        <li className={`list-item ${className}__list-item`}>
            <div className={`list-item__image-block ${className}__image-block`}>
                <Thumbnail image={image} />
            </div>
            <div className={`list-item__description-block ${className}__description-block`}>
                <Title>{title}</Title>
                <Text>{description}</Text>
            </div>
            <div className="list-item__details-block">{children}</div>
        </li>
    );
}

export default ProductItem;
