import React from "react";

function Thumbnail({ image }) {
    return <img src={image} alt="Something went wrong!" className="thumbnail" />;
}

export default Thumbnail;
