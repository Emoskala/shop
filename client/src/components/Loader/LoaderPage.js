import React from "react";

function LoaderPage() {
    return (
        <div className="loader-block">
            <div className="loader">
                <div className="loader__loader-element"></div>
                <div className="loader__loader-element"></div>
                <div className="loader__loader-element"></div>
            </div>
        </div>
    );
}

export default LoaderPage;
