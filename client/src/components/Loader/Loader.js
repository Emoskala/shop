import React from "react";
import LoaderPage from "./LoaderPage";
import { useSelector } from "react-redux";

/**
 * Creates Loader component which handles pending requests.
 */
function Loader() {
    const state = useSelector((state) => state);

    return (
        <>
            {state.user.pending && <LoaderPage />}
            {state.shop.pending && <LoaderPage />}
        </>
    );
}

export default Loader;
