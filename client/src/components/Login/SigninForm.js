import React from "react";
import Form from "./Form";
import { signInUser } from "../../middlewares/login";

function SigninForm() {
    return <Form requestMethod={signInUser} title={"Login form"} buttonContent={"Login"} />;
}
export default SigninForm;
