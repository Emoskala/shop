import React, { useState } from "react";
import PrimaryButton from "../Buttons/PrimaryButton";
import Input from "../Input/Input";
import Text from "../Text/Text";
import Title from "../Title/Title";
import { useDispatch } from "react-redux";

function Form({ requestMethod, title, buttonContent }) {
    const [loginValue, setLoginValue] = useState("");
    const [passwordValue, setPasswordValue] = useState("");
    const dispatch = useDispatch();

    /**
     * changes actual value of state.
     */
    const handleLoginChange = (event) => {
        setLoginValue(event.currentTarget.value);
    };

    /**
     * changes actual value of state.
     */
    const handlePasswordChange = (event) => {
        setPasswordValue(event.currentTarget.value);
    };

    /**
     * handles form submit.
     */
    const handleSubmit = (event) => {
        event.preventDefault();
        dispatch(requestMethod(loginValue, passwordValue));
    };

    return (
        <form onSubmit={handleSubmit} className="login__form">
            <Title>{title}</Title>
            <div className="login__inputs-block">
                <Input type="login" name="username" className="login__input" placeholder="Username" value={loginValue} onChange={handleLoginChange} />
                <Input type="password" name="password" className="login__input" placeholder="Password" value={passwordValue} onChange={handlePasswordChange} />
            </div>
            <PrimaryButton type="submit" className="login__button">
                <Text>{buttonContent}</Text>
            </PrimaryButton>
        </form>
    );
}
export default Form;
