import React from "react";
import Form from "./Form";
import { registerUser } from "../../middlewares/login";

function RegisterForm() {
    return <Form requestMethod={registerUser} title={"Register form"} buttonContent={"Register"} />;
}
export default RegisterForm;
