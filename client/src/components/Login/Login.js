import React from "react";
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
import LoginRouterMenu from "./LoginRouterMenu";
import MenuItem from "./MenuItem";
import SigninForm from "./SigninForm";
import RegistrationForm from "./RegistrationForm";
import Text from "../Text/Text";

function Login() {
    return (
        <div className="login">
            <Router>
                <LoginRouterMenu>
                    <MenuItem className="login__menu-item" path="/signin">
                        <Text>Sign In</Text>
                    </MenuItem>
                    <MenuItem className="login__menu-item" path="/register">
                        <Text>Register</Text>
                    </MenuItem>
                </LoginRouterMenu>
                <Switch>
                    <Route exact path="/signin">
                        <SigninForm />
                    </Route>
                    <Route exact path="/register">
                        <RegistrationForm />
                    </Route>
                    <Route path="/">
                        <Redirect to="/signin" />
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default Login;
