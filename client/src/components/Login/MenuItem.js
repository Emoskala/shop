import React from "react";
import { Link, useLocation } from "react-router-dom";
import Title from "../Title/Title";

function MenuItem({ children, path, className }) {
    const location = useLocation();

    return (
        <li className={path === location.pathname ? `${className} ${className}--active` : className}>
            <Link to={path}>
                <Title>{children}</Title>
            </Link>
        </li>
    );
}

export default MenuItem;
