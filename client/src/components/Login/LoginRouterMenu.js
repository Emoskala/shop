import React from "react";

function LoginRouterMenu({ children }) {
    return (
        <nav className="login__menu">
            <ul className="login__list">{children}</ul>
        </nav>
    );
}

export default LoginRouterMenu;
