import React from "react";

function PrimaryButton({ onClick, className, children, type }) {
    return (
        <button type={type} className={`primary-button ${className}`} onClick={onClick}>
            {children}
        </button>
    );
}

export default PrimaryButton;
