import React from "react";
import { FaShoppingBasket } from "react-icons/fa";

function BasketButton({ onClick, className }) {
    return (
        <button className={`basket-button ${className}`} onClick={onClick}>
            <FaShoppingBasket />
        </button>
    );
}

export default BasketButton;
