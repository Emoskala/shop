import React from "react";
import { FaTrashAlt } from "react-icons/fa";

function TrashButton({ onClick, className }) {
    return (
        <button className={`trash-button ${className}`} onClick={onClick}>
            <FaTrashAlt />
        </button>
    );
}

export default TrashButton;
