import React from "react";
import { AiOutlineLogout } from "react-icons/ai";

function LogoutButton({ onClick, className }) {
    return (
        <button className={`logout-button ${className}`} onClick={onClick}>
            <AiOutlineLogout />
        </button>
    );
}

export default LogoutButton;
