import React from "react";
import { AiOutlineEdit } from "react-icons/ai";

function EditButton({ onClick, className }) {
    return (
        <button className={`edit-button ${className}`} onClick={onClick}>
            <AiOutlineEdit />
        </button>
    );
}

export default EditButton;
