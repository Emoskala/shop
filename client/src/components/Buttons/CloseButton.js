import React from "react";
import { AiOutlineClose } from "react-icons/ai";

function CloseButton({ onClick, className }) {
    return (
        <button className={`close-button ${className}`} onClick={onClick}>
            <AiOutlineClose />
        </button>
    );
}

export default CloseButton;
