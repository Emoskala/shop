import React from "react";
import Text from "../Text/Text";

function Price({ children }) {
    const unit = "zł";
    return (
        <Text>
            {children}
            {unit}
        </Text>
    );
}

export default Price;
