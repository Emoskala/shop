import React from "react";

function Image({ image }) {
    return <img src={image} alt="Something went wrong!" className="image" />;
}

export default Image;
