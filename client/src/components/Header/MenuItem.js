import React from "react";
import { Link } from "react-router-dom";
import Title from "../Title/Title";

function MenuItem({ LinkTo, children, active }) {
    return (
        <li className={`header__menu-item ${active && "header__menu-item--active"}`}>
            <Link className={`header__link`} to={LinkTo}>
                <Title>{children}</Title>
            </Link>
        </li>
    );
}

export default MenuItem;
