import React from "react";

function Menu({ children }) {
    return (
        <nav className="header__menu">
            <ul className="header__list">{children}</ul>
        </nav>
    );
}

export default Menu;
