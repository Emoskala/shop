import React from "react";
import { Switch } from "react-router-dom";

function MenuSwitch({ children }) {
    return <Switch>{children}</Switch>;
}

export default MenuSwitch;
