import { configureStore } from "@reduxjs/toolkit";
import { rootReducer } from "../reducers/rootReducer";
import thunk from "redux-thunk";

/**
 * Creates a store.
 */
export const store = configureStore({ reducer: rootReducer, middleware: [thunk] });
