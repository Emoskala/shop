import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
    name: "user",
    initialState: {
        username: null,
        loggedIn: false,
        error: false,
        pending: false,
    },
    reducers: {
        login: (state, action) => {
            return {
                ...state,
                username: action.payload,
                pending: false,
                loggedIn: true,
            };
        },
        logout: (state, action) => {
            return {
                ...state,
                username: null,
                loggedIn: false,
            };
        },
        loginPending: (state, action) => {
            return {
                ...state,
                pending: true,
            };
        },
        loginError: (state, action) => {
            return {
                ...state,
                pending: false,
                error: action.payload,
            };
        },
        loginDisableError: (state, action) => {
            return {
                ...state,
                error: false,
            };
        },
    },
});
