import { createSlice } from "@reduxjs/toolkit";

export const shopSlice = createSlice({
    name: "shop",
    initialState: {
        products: [],
        pending: false,
        error: false,
    },
    reducers: {
        addShopItems: (state, action) => {
            return {
                ...state,
                products: [...action.payload],
                pending: false,
                error: false,
            };
        },
        addShopItemsPending: (state, action) => {
            return {
                ...state,
                pending: true,
            };
        },
        addShopItemsError: (state, action) => {
            return {
                ...state,
                pending: false,
                error: action.payload,
            };
        },
        addShopItemsDisableError: (state, action) => {
            return {
                ...state,
                error: false,
            };
        },
    },
});
