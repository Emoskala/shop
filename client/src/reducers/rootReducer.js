import { combineReducers } from "redux";
import { shopSlice } from "./shopSlice";
import { basketSlice } from "./basketSlice";
import { userSlice } from "./userSlice";

/**
 * A rootReducer which contains all reducers.
 */
export const rootReducer = combineReducers({ shop: shopSlice.reducer, basket: basketSlice.reducer, user: userSlice.reducer });
