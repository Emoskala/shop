import { createSlice } from "@reduxjs/toolkit";
import { getBasketItemId } from "../selectors/shopSelector";

export const basketSlice = createSlice({
    name: "basket",
    initialState: [],
    reducers: {
        addItemToBasket,
        removeItemFromBasket,
    },
});

/**
 * Adds item to basket if item not exists.
 * Otherwise removes basket item from state and adds a new one, with new values.
 */
function addItemToBasket(state, action) {
    const idIfExists = getBasketItemId(state, action.payload._id);
    if (idIfExists === -1) {
        return [...state, action.payload];
    } else {
        const newState = [...state];
        newState.splice(idIfExists, 1);
        return [...newState, action.payload];
    }
}

/**
 * Removes a basket item from state.
 */
function removeItemFromBasket(state, action) {
    const idIfExists = getBasketItemId(state, action.payload);
    if (idIfExists === -1) {
        return [...state];
    } else {
        const newState = [...state];
        newState.splice(idIfExists, 1);
        return [...newState];
    }
}
