# App still in work.

## MongoDB.

Before starting, you have to have running MongoDB at default port (27017).

## Installation.

You have to install dependencies for client and server.

### `npm install`

## Running

You have to run client and server.

### `npm start`

You have a running app, so you can:
-Register new user
-sign in.

## Items in shop.

To add items to shop, because it's empty now, you have to add to collection "shopitems" documents.

Documents examples:
{
"title":"Table",
"description":"Elegant furniture for living room",
"image":"https://a.allegroimg.com/s512/036ed9/a0431d614e57996f41302559a91f/Stol-rozkladany-160-260-LOFT-metalowe-nogi",
"unitPrice":149.99
}

{
"title":"Chair",
"description":"Elegant furniture for living room",
"image":"https://images.homebase.co.uk/Product-1600x1600/28c38ffc-4744-4695-a6d8-8f7f8d8d4d56.jpg",
"unitPrice":99.99
}

you can copy those documents and add to your MongoDB.
